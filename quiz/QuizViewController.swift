//
//  QuizViewController.swift
//  quiz
//
//  Created by mac on 28.01.2021.
//

import UIKit

class QuizViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    var quizModels = [Question]()
    
    var currentQuestion: Question?
    
    @IBOutlet var label: UILabel!
    @IBOutlet var table: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        table.delegate = self
        table.dataSource = self
        setupQuestions()
        configureUI(question:quizModels.first!)
    }
        

    private func configureUI(question: Question) {
        label.text = question.text
        currentQuestion = question
        table.reloadData()
    }
    
    private func checkAnswer(answer: Answer, question: Question) -> Bool {
        return question.answers.contains(where: { $0.text == answer.text}) && answer.correct
    }
    
    private func setupQuestions() {
        quizModels.append(Question(text: "When JB was born?", answers: [
            Answer(text: "1 March 1994", correct: true),
            Answer(text: "1 March 1995", correct: false),
            Answer(text: "1 March 1996", correct: false),
            Answer(text: "1 March 1997", correct: false),
        ]))
        
        quizModels.append(Question(text: "Where JB was born?", answers: [
            Answer(text: "Toronto, Canada", correct: false),
            Answer(text: "Ontario, Canada", correct: true),
            Answer(text: "New York, USA", correct: false),
            Answer(text: "Los Angeles, USA", correct: false),
        ]))
        
        quizModels.append(Question(text: "How JB became famous?", answers: [
            Answer(text: "He is from powerful family", correct: false),
            Answer(text: "He was noticed by musicians", correct: false),
            Answer(text: "He recorded video for YT", correct: true),
            Answer(text: "He is illuminati", correct: false),
        ]))
        
        quizModels.append(Question(text: "JB's middle name?", answers: [
            Answer(text: "Charles", correct: false),
            Answer(text: "Hudson", correct: false),
            Answer(text: "Wells", correct: false),
            Answer(text: "Drew", correct: true),
        ]))
        
        quizModels.append(Question(text: "JB's very first hit song?", answers: [
            Answer(text: "One time", correct: false),
            Answer(text: "Favourite girl", correct: false),
            Answer(text: "Baby", correct: true),
            Answer(text: "Boyfriend", correct: false),
        ]))
        
        quizModels.append(Question(text: "JB's fav color?", answers: [
            Answer(text: "purple", correct: true),
            Answer(text: "red", correct: false),
            Answer(text: "cyan", correct: false),
            Answer(text: "black", correct: false),
        ]))
        
        quizModels.append(Question(text: "How many albums JB has released?", answers: [
            Answer(text: "7", correct: false),
            Answer(text: "5", correct: true),
            Answer(text: "4", correct: false),
            Answer(text: "6", correct: false),
        ]))
        
        quizModels.append(Question(text: "How many siblings does JB has?", answers: [
            Answer(text: "5", correct: false),
            Answer(text: "7", correct: false),
            Answer(text: "3", correct: false),
            Answer(text: "4", correct: true),
        ]))
        
        quizModels.append(Question(text: "JB's wife?", answers: [
            Answer(text: "Hailey Baldwin", correct: true),
            Answer(text: "Selena Gomez", correct: false),
            Answer(text: "Irina Shayk", correct: false),
            Answer(text: "Miley Cyrus", correct: false),
        ]))
        
        quizModels.append(Question(text: "How many tattoos JB has?", answers: [
            Answer(text: "0", correct: false),
            Answer(text: "54", correct: false),
            Answer(text: "60+", correct: true),
            Answer(text: "30", correct: false),
        ]))
    }
    
    // table view functions
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return currentQuestion?.answers.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.textLabel?.text = currentQuestion?.answers[indexPath.row].text
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        guard let question = currentQuestion else {
            return
        }
        let answer = question.answers[indexPath.row]
        
        if checkAnswer(answer: answer, question: question) {
            // correct
            if let index = quizModels.firstIndex(where: { $0.text == question.text }) {
                if index < (quizModels.count - 1) {
                    // next question
                    let nextQuestion = quizModels[index + 1]
                    print ("\(nextQuestion.text)")
                    currentQuestion = nil
                    configureUI(question: nextQuestion)
                } else {
                    // end of quiz
                    let alert = UIAlertController(title: "Done", message: "You are a true Belieber!", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler: nil))
                    present(alert, animated:true)
                }
            }
        }
        else {
            // wrong
            let alert = UIAlertController(title: "Wrong", message: "You are not a true Belieber", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler: nil))
            present(alert, animated:true)
        }
    }
    
    
    struct Question {
        let text: String
        let answers: [Answer]
    }
    
    struct Answer {
        let text: String
        let correct: Bool
    }

}
