//
//  ViewController.swift
//  quiz
//
//  Created by mac on 28.01.2021.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func StartGame() {
        let vc = storyboard?.instantiateViewController(identifier: "quiz") as! QuizViewController
        vc.modalPresentationStyle = .fullScreen
        present (vc, animated: true)
    }


}

